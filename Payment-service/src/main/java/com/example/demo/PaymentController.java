package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/utp")
public class PaymentController {

	@GetMapping("/payment/{price}")
	public String getPayment(@PathVariable int price) {
		return "Payment Amount : " + price + "Successullly!";
	}
}
